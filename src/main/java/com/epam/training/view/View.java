package com.epam.training.view;

import com.epam.training.config.*;
import com.epam.training.config.profile.AdditionalProfileConfig;
import com.epam.training.config.profile.MainProfileConfig;
import com.epam.training.model.animal.Animal;
import com.epam.training.model.animal.AnimalGroup;
import com.epam.training.model.animal.AnotherGroup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

import java.util.*;

public class View {
    private static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    private ResourceBundle bundle;
    private ApplicationContext context;
    private AnnotationConfigApplicationContext profileContext;

    public View() {
        bundle = ResourceBundle.getBundle("Menu");
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        context = new AnnotationConfigApplicationContext(OthersConfig.class,
                Beans1Config.class, FlowersConfig.class, Task8Config.class
                );
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::showBeansName);
        methodsMenu.put("2", this::showAnimals);
        methodsMenu.put("3", this::showOthers);
        methodsMenu.put("4", this::checkMainProfile);
        methodsMenu.put("5", this::checkAdditionalProfile);
        outputMenu();
    }

    private void showBeansName() {
        for (String beanName: context.getBeanDefinitionNames()) {
            logger.trace(context.getBean(beanName).getClass().toString());
        }
    }

    private void showAnimals() {
        AnimalGroup animals = (AnimalGroup) context.getBean("animalGroup");
        animals.getAnimals().forEach(animal -> logger.info(animal.getAnimal()));
    }

    private void showOthers(){
        logger.info(context.getBean("otherBeanA").toString());
        logger.info(context.getBean("otherBeanA").toString());
        logger.info(context.getBean("otherBeanB").toString());
        logger.info(context.getBean("otherBeanB").toString());
        logger.info(context.getBean("otherBeanC").toString());
        logger.info(context.getBean("otherBeanC").toString());
        logger.info(((Animal)context.getBean("sparrow")).getAnimal());
        logger.info(((Animal)context.getBean("sparrow")).getAnimal());
    }

    private void checkMainProfile(){
        profileContext =
                new AnnotationConfigApplicationContext();
        profileContext.getEnvironment().setActiveProfiles("mainProfile");
        profileContext.register(MainProfileConfig.class);
        profileContext.refresh();
        Arrays.stream(profileContext.getBeanDefinitionNames()).forEach(logger::info);
    }

    private void checkAdditionalProfile(){
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "additionalProfile");
        profileContext =
                new AnnotationConfigApplicationContext(AdditionalProfileConfig.class);
        Arrays.stream(profileContext.getBeanDefinitionNames()).forEach(logger::info);
    }

    private void outputMenu() {
        logger.trace("MENU:");
        for (String option : menu.values()) {
            logger.trace(option);
        }
        logger.trace(">>> ");
    }
}
