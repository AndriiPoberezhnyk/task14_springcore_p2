package com.epam.training.model.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private Color color;

    public RoseFlower() {
        color = Color.RED;
    }

    public RoseFlower(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "color=" + color +
                '}';
    }
}
