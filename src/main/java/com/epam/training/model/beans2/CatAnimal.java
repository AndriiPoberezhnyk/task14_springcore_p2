package com.epam.training.model.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String name;
    private int age;

    public CatAnimal() {
        name = "Cat";
        age = 0;
    }

    public CatAnimal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
