package com.epam.training.model.beans2;

public enum Color {
    BLUE, GREEN, YELLOW, RED, ORANGE, WHITE
}
