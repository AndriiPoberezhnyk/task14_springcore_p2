package com.epam.training.model.other;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class OtherBeanA {
    private String name;
    private int value;

    public OtherBeanA() {
        name = "OtherBeanWithMaxValue";
        value = Integer.MAX_VALUE;
    }

    public OtherBeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}' + hashCode();
    }
}
