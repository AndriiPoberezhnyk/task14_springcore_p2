package com.epam.training.model.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Qualifier("otherB")
public class OtherBeanB {
    private String name;
    private int value;

    public OtherBeanB() {
        name = "OtherWithMinValue";
        value = Integer.MIN_VALUE;
    }

    public OtherBeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}' + hashCode();
    }
}
