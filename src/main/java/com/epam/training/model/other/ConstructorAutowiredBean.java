package com.epam.training.model.other;

import com.epam.training.model.beans1.BeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConstructorAutowiredBean {
    private OtherBeanA otherBeanA;
    private BeanA beanA;

    @Autowired
    public ConstructorAutowiredBean(OtherBeanA otherBeanA, BeanA beanA) {
        this.otherBeanA = otherBeanA;
        this.beanA = beanA;
    }

    @Override
    public String toString() {
        return "ConstructorAutowiredBean{" +
                "otherBeanA=" + otherBeanA +
                ", beanA=" + beanA +
                '}';
    }
}
