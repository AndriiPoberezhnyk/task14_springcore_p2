package com.epam.training.model.animal;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("bird")
public class Bird implements Animal {
    private String type;

    public Bird() {
        setType();
    }

    public Bird(String type) {
        this.type = type;
    }

    private void setType() {
        this.type = "bird";
    }

    @Override
    public String getAnimal() {
        return type +", "+ hashCode();
    }
}
