package com.epam.training.model.animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AnimalGroup {
    @Autowired
    private List<Animal> animals;

    public List<Animal> getAnimals() {
        return animals;
    }
}
