package com.epam.training.model.animal;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal {
    private String type;

    public Dog() {
        this.type = "dog";
    }

    @Override
    public String getAnimal() {
        return type;
    }
}
