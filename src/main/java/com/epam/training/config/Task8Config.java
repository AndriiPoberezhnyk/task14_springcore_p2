package com.epam.training.config;

import com.epam.training.model.animal.AnotherGroup;
import com.epam.training.model.animal.Bird;

import com.epam.training.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan("com.epam.training.model.animal")
public class Task8Config {
    private static final Logger logger = LogManager.getLogger(View.class.getName());

    @Bean
    @Primary
    public AnotherGroup getBeanX(){
        return new AnotherGroup();
    }

    @Bean("sparrow")
    @Scope("singleton")
    public Bird getOtherBird(){
        return new Bird("Sparrow");
    }
}
