package com.epam.training.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.training.model.beans2",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(
                        type = FilterType.REGEX, pattern = ".*Flower$")),
        @ComponentScan(basePackages = "com.epam.training.model.beans3",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(
                        type = FilterType.REGEX, pattern = ".*[DF]$"))
})
public class FlowersConfig {
}
