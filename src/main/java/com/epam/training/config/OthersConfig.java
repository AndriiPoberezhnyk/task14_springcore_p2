package com.epam.training.config;

import com.epam.training.model.beans1.BeanA;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScans({
        @ComponentScan("com.epam.training.model.other"),
        @ComponentScan(basePackages = "com.epam.training.model.beans1",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(type =
                        FilterType.ASSIGNABLE_TYPE, classes = BeanA.class))
})

public class OthersConfig {
}
