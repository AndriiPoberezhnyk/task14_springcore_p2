package com.epam.training.config.profile;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan("com.epam.training.model")
@Profile("mainProfile")
public class MainProfileConfig {
}
