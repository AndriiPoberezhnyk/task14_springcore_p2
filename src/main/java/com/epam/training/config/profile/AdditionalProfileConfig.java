package com.epam.training.config.profile;

import com.epam.training.model.animal.AnimalGroup;
import com.epam.training.model.animal.Cat;
import com.epam.training.model.beans2.NarcissusFlower;
import com.epam.training.model.other.FieldAutowiredBean;
import com.epam.training.model.other.OtherBeanB;
import org.springframework.context.annotation.*;

@Configuration
@Profile("additionalProfile")
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.training.model.animal",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(type =
                        FilterType.ASSIGNABLE_TYPE, classes =
                        Cat.class)),
        @ComponentScan(basePackages = "com.epam.training.model.other",
                useDefaultFilters = false,
                includeFilters = @ComponentScan.Filter(type =
                        FilterType.ASSIGNABLE_TYPE, classes =
                        OtherBeanB.class)),
})
public class AdditionalProfileConfig {
    @Bean
    public AnimalGroup getAnimalGroup(){
        return new AnimalGroup();
    }

    @Bean
    public NarcissusFlower getNarcissusFlower(){
        return new NarcissusFlower();
    }

    @Bean
    public FieldAutowiredBean getFieldBean(){
        return new FieldAutowiredBean();
    }
}
